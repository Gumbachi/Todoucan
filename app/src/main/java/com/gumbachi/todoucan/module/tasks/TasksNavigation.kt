package com.gumbachi.todoucan.module.tasks

import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable

fun NavGraphBuilder.tasksScreen(
    onMenuClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    composable("tasks") {
        TasksScreen(
            onMenuClick = onMenuClick,
            modifier = modifier
        )
    }
}

fun NavController.navigateToTasksScreen() {
     navigate("tasks") {
         popUpTo(0) {
             inclusive = true
             saveState = true
         }
         restoreState = true
         launchSingleTop = true
     }
}