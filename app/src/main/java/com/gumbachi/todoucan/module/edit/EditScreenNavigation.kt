package com.gumbachi.todoucan.module.edit

import android.util.Log
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import org.koin.androidx.compose.koinViewModel

private const val TAG = "EDIT-NAVIGATION"

fun NavGraphBuilder.editScreen(
    onMenuClick: () -> Unit,
    navigateBack: () -> Unit,
    modifier: Modifier = Modifier
) {
    composable(
        route = "edit?filepath={filepath}",
        arguments = listOf(
            navArgument(name = "filepath") { type = NavType.StringType }
        )
    ) {
        it.arguments?.let { args ->
            EditScreen(
                modifier = modifier,
                viewModel = koinViewModel(),
                onMenuClick = onMenuClick,
                filepath = args.getString("filepath"),
                navigateBack = navigateBack
            )
        }
    }
}

fun NavController.navigateToEditScreen(filepath: String) {
    Log.d(TAG, "Navigating to edit/$filepath")
    navigate("edit?filepath=$filepath") {
        restoreState = false
        launchSingleTop = true
    }

}