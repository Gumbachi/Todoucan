package com.gumbachi.todoucan.module.settings

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    modifier: Modifier = Modifier,
    viewModel: SettingsScreenViewModel = koinViewModel(),
    onMenuClick: () -> Unit = {}
) {
    Scaffold(
        modifier = modifier,
        topBar = {
            CenterAlignedTopAppBar(
                title = { Text("Settings") },
                navigationIcon = {
                    IconButton(onClick = onMenuClick) {
                        Icon(
                            imageVector = Icons.Default.Menu,
                            contentDescription = "Open Navigation Drawer"
                        )
                    }
                }
            )
        }
    ) {
        Column(
            modifier = Modifier
                .padding(it)
                .verticalScroll(rememberScrollState())
        ) {

            SectionHeader(label = "General")

            SettingsSwitch(
                label = "Show file type extension",
                checked = false,
                onCheckedChange = {},
                icon = Icons.Default.Check
            )
            SettingsSwitch(
                label = "Setting Two",
                checked = true,
                onCheckedChange = {},
                description = "Setting two has a rather long description that extends further than it should and might even span more than two lines this looks so bad"
            )
            SettingsSwitch(
                label = "Setting Three",
                checked = false,
                onCheckedChange = {},
                icon = Icons.Default.Star
            )
            SettingsSwitch(label = "Setting Four", checked = true, onCheckedChange = {})
            Spacer(modifier = Modifier.padding(vertical = 12.dp))

            SectionHeader(label = "Layout")
            SettingsSwitch(label = "Setting One", checked = false, onCheckedChange = {})
            SettingsSwitch(label = "Setting Two", checked = true, onCheckedChange = {})
            SettingsSwitch(label = "Setting Three", checked = false, onCheckedChange = {})
            SettingsSwitch(label = "Setting Four", checked = true, onCheckedChange = {})
            Spacer(modifier = Modifier.padding(vertical = 12.dp))

            SectionHeader(label = "Miscellaneous")
            SettingsSwitch(label = "Setting One", checked = false, onCheckedChange = {})
            SettingsSwitch(label = "Setting Two", checked = true, onCheckedChange = {})
            SettingsSwitch(label = "Setting Three", checked = false, onCheckedChange = {})
            SettingsSwitch(label = "Setting Four", checked = true, onCheckedChange = {})
        }

    }
}

@Composable
private fun SettingsSwitch(
    label: String,
    checked: Boolean,
    onCheckedChange: (Boolean) -> Unit,
    modifier: Modifier = Modifier,
    description: String? = null,
    icon: ImageVector? = null,
    iconContentDescription: String? = null
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 4.dp)
    ) {

        Row(verticalAlignment = Alignment.CenterVertically) {
            if (icon != null) {
                Icon(imageVector = icon, contentDescription = iconContentDescription)
                Spacer(modifier = Modifier.padding(end = 12.dp))
            }

            Column {
                Text(label, style = MaterialTheme.typography.bodyLarge)
                if (description != null)
                    Text(
                        description,
                        style = MaterialTheme.typography.bodySmall,
                        color = MaterialTheme.colorScheme.onSurfaceVariant,
                        modifier = Modifier.fillMaxWidth(0.75F)
                    )
            }
        }
        Switch(
            checked = checked,
            onCheckedChange = onCheckedChange
        )

    }
}

@Composable
private fun SectionHeader(
    label: String,
    modifier: Modifier = Modifier
) {
    Row(verticalAlignment = Alignment.CenterVertically, modifier = modifier.padding(start = 12.dp)) {
        Text(
            text = label,
            style = MaterialTheme.typography.headlineSmall
        )
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp), thickness = 2.dp
        )
    }
}

