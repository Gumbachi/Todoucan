package com.gumbachi.todoucan.module.notes

import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.gumbachi.todoucan.navigation.TodoucanRoutes
import org.koin.androidx.compose.koinViewModel

fun NavGraphBuilder.notesScreen(
    onMenuClick: () -> Unit,
    navigateToEditScreen: (filename: String) -> Unit,
    modifier: Modifier = Modifier
) {
    composable(TodoucanRoutes.Notes.route) {
        NotesScreen(
            modifier = modifier,
            viewModel = koinViewModel(),
            onMenuClick = onMenuClick,
            navigateToEditScreen = navigateToEditScreen
        )
    }
}

fun NavController.navigateToNotesScreen() {
     navigate(TodoucanRoutes.Notes.route) {
         popUpTo(0) {
             inclusive = true
             saveState = true
         }
         restoreState = true
         launchSingleTop = true
     }
}