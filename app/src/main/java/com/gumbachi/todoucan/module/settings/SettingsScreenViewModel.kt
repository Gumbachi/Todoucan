package com.gumbachi.todoucan.module.settings

import androidx.lifecycle.ViewModel

data class SettingsScreenState(
    val error: Throwable? = null
)

class SettingsScreenViewModel : ViewModel() {

}