package com.gumbachi.todoucan.module.settings

import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.gumbachi.todoucan.navigation.TodoucanRoutes
import org.koin.androidx.compose.koinViewModel

fun NavGraphBuilder.settingsScreen(
    onMenuClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    composable(TodoucanRoutes.Settings.route) {
        SettingsScreen(
            modifier = modifier,
            viewModel = koinViewModel(),
            onMenuClick = onMenuClick
        )
    }
}

fun NavController.navigateToSettingsScreen() {
     navigate(TodoucanRoutes.Settings.route) {
         popUpTo(0) {
             inclusive = true
             saveState = true
         }
         restoreState = true
         launchSingleTop = true
     }
}