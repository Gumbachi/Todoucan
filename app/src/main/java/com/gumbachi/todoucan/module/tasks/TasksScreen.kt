package com.gumbachi.todoucan.module.tasks

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import org.koin.androidx.compose.koinViewModel

private const val TAG = "TASKS-SCREEN"

@Composable
fun TasksScreen(
    modifier: Modifier = Modifier,
    viewModel: TasksScreenViewModel = koinViewModel(),
    onMenuClick: () -> Unit
) {

    val context = LocalContext.current
    val state by viewModel.state.collectAsState()

    Scaffold(
        modifier = modifier,
        bottomBar = {
            BottomAppBar {
                IconButton(onClick = onMenuClick) {
                    Icon(
                        imageVector = Icons.Default.Menu,
                        contentDescription = "Open Navigation"
                    )
                }
            }
        }
    ) { paddingValues ->
        Column(modifier = Modifier.padding(paddingValues)) {
            Text("Tasks Go Here")
        }
    }
}



