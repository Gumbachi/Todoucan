package com.gumbachi.todoucan.module.edit

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SegmentedButton
import androidx.compose.material3.SegmentedButtonDefaults
import androidx.compose.material3.SingleChoiceSegmentedButtonRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.gumbachi.todoucan.data.model.enums.EditMode
import com.gumbachi.todoucan.ui.components.InteractiveView
import com.gumbachi.todoucan.ui.components.Toolbox
import com.gumbachi.todoucan.ui.components.dialog.ErrorDialog
import com.gumbachi.todoucan.ui.components.dialog.SaveDialog
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditScreen(
    filepath: String?,
    modifier: Modifier = Modifier,
    viewModel: EditScreenViewModel = koinViewModel(),
    onMenuClick: () -> Unit = {},
    navigateBack: () -> Unit
) {

    val context = LocalContext.current
    val state by viewModel.state.collectAsState()
    val scope = rememberCoroutineScope()

    LaunchedEffect(filepath) {
        if (filepath != null) {
            viewModel.openFile(context = context, filepath = filepath)
        } else {
            viewModel.setError(Exception("filepath was null"))
        }
    }

    BackHandler(enabled = !state.isSaved) {
        viewModel.showSaveDialog()
    }

    SaveDialog(
        showIf = state.showSaveDialog,
        onDismissRequest = { viewModel.hideSaveDialog() },
        onConfirmSave = {
            viewModel.saveFile(context = context)
            viewModel.hideSaveDialog()
            if (state.error == null) {
                navigateBack()
            }

        },
        onDismissSave = {
            viewModel.hideSaveDialog()
            navigateBack()
        }
    )

    state.error?.let {
        ErrorDialog(
            error = state.error!!,
            onDismissRequest = {},
            onConfirm = { viewModel.setError(null) }
        )
    }


    Scaffold(
        modifier = modifier,
        bottomBar = {
            BottomAppBar(modifier = Modifier.imePadding()) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                )  {
                    IconButton(onClick = {
                        when (state.isSaved) {
                            true -> navigateBack()
                            false -> viewModel.showSaveDialog()
                        }
                    }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = "Go Back"
                        )
                    }

                    Row {

                        SingleChoiceSegmentedButtonRow {
                            EditMode.entries.forEachIndexed { index, mode ->
                                SegmentedButton(
                                    selected = state.currentEditMode == mode,
                                    onClick = { viewModel.setEditMode(mode) },
                                    shape = SegmentedButtonDefaults.itemShape(index = index, count = EditMode.entries.size)
                                ) {
                                    Icon(imageVector = mode.icon, contentDescription = null)
                                }
                            }
                        }


                        IconButton(onClick = { viewModel.saveFile(context = context) }) {
                            Icon(imageVector = Icons.Default.Save, contentDescription = "save file")
                        }
                        IconButton(onClick = {
                            viewModel.deleteFile(context = context)
                            navigateBack()
                        }) {
                            Icon(
                                imageVector = Icons.Default.Delete,
                                contentDescription = "Cycle Mode"
                            )
                        }
                    }
                }
            }
        }
    ) { paddingValues ->

        Box(modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)
        ) {

            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {

                // Title Text field
                TextField(
                    value = state.title,
                    onValueChange = { viewModel.updateTitle(it) },
                    modifier = Modifier.fillMaxWidth(),
                    placeholder = {
                        Text(
                            text = "Title...",
                            style = MaterialTheme.typography.titleLarge,
                            color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.5F)
                        )
                    },
                    colors = TextFieldDefaults.colors(
                        unfocusedContainerColor = Color.Transparent,

                        ),
                    textStyle = MaterialTheme.typography.titleLarge,
                    singleLine = true
                )
                
                when (state.currentEditMode) {
                    EditMode.Edit -> {
                        TextField(
                            value = state.body,
                            onValueChange = { viewModel.updateText(it) },
                            modifier = Modifier.fillMaxWidth(),
                            placeholder = {
                                Text(
                                    text = "Your deepest thoughts go here...",
                                    style = MaterialTheme.typography.bodyMedium,
                                    color = MaterialTheme.colorScheme.onBackground.copy(alpha = 0.5F)
                                )
                            },
                            colors = TextFieldDefaults.colors(unfocusedContainerColor = Color.Transparent),
                            textStyle = MaterialTheme.typography.bodyMedium
                        )
                    }
                    EditMode.Read -> {
                        InteractiveView(
                            content = state.body.text,
                            onStrikethroughClick = { viewModel.toggleStrikethrough(it) },
                            onDeleteLineClick = { viewModel.deleteLine(it) },
                            onToggleCheckboxClick = { viewModel.toggleCheckbox(it) }
                        )
                    }
                }
            }

            Toolbox(
                expanded = state.showToolbox,
                onExpandClick = viewModel::toggleToolbox,
                modifier = Modifier.align(Alignment.BottomCenter)
            )

        }
    }
}
