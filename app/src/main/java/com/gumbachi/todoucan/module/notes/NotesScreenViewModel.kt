package com.gumbachi.todoucan.module.notes

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.gumbachi.todoucan.data.model.TodoucanFiles
import com.gumbachi.todoucan.util.fileio.Writer
import com.gumbachi.todoucan.util.fileio.getFileMap
import com.gumbachi.todoucan.util.fileio.getFileTree
import com.gumbachi.todoucan.util.fileio.getRelativePath
import com.gumbachi.todoucan.util.fileio.homeDir
import com.gumbachi.todoucan.util.fileio.withNewName
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.io.File

private const val TAG = "NOTES-VM"

data class FolderEditState(
    val show: Boolean = false,
    val originalFolder: File? = null,
    val value: String = "",
    val error: Throwable? = null,
)

data class FileEditState(
    val show: Boolean = false,
    val originalFile: File? = null,
    val value: String = "",
    val error: Throwable? = null,
)

data class NewFolderState(
    val show: Boolean = false,
    val value: String = "",
    val error: Throwable? = null,
)

data class NotesScreenState(
    val loading: Boolean = true,
    val error: Throwable? = null,

    // Files
    val files: TodoucanFiles? = null,
    val fileTree: FileTreeWalk? = null,
    val fileMap: Map<File, List<File>> = emptyMap(),


    // New File Bottom Sheet
    val showNewFileBottomSheet: Boolean = false,
)

class NotesScreenViewModel : ViewModel() {

    //region Folder Edit State
    private val _folderEditState = MutableStateFlow(FolderEditState())
    val folderEditState = _folderEditState.asStateFlow()

    fun showFolderEditDialogFor(dir: File) = _folderEditState.update {
        it.copy(show = true, originalFolder = dir, value = dir.name)
    }

    fun hideFolderEditDialog() = _folderEditState.update {
        it.copy(show = false, originalFolder = null, value = "", error = null)
    }

    fun updateFolderEditValue(value: String) = _folderEditState.update {
        it.copy(value = value)
    }

    fun updateFolderEditError(error: Throwable?) = _folderEditState.update {
        it.copy(error = error)
    }

    fun deleteFolder(context: Context) {
        runCatching {
            Writer.deleteDirectory(
                context = context,
                filepath = _folderEditState.value.originalFolder!!.getRelativePath(context),
                deleteRecursively = true
            )
        }.onSuccess {
            updateFiles(context = context)
        }.onFailure {
            setError(it)
        }
    }

    fun renameFolder(context: Context) {
        runCatching {
            val originalFolder = _folderEditState.value.originalFolder!!
            val newFolderName = _folderEditState.value.value

            val oldPath = originalFolder.getRelativePath(context)
            val newPath = oldPath.replace(originalFolder.name, newFolderName)

            Writer.renameFile(context = context, oldPath = oldPath, newPath = newPath)
        }.onSuccess {
            updateFiles(context = context)
        }.onFailure {
            setError(it)
        }
    }

    //endregion

    //region File Edit State
    private val _fileEditState = MutableStateFlow(FileEditState())
    val fileEditState = _fileEditState.asStateFlow()

    fun showFileEditDialogFor(file: File) = _fileEditState.update {
        it.copy(show = true, originalFile = file, value = file.name)
    }

    fun hideFileEditDialog() = _fileEditState.update {
        it.copy(show = false, originalFile = null, value = "", error = null)
    }

    fun updateFileEditValue(value: String) = _fileEditState.update {
        it.copy(value = value)
    }

    fun updateFileEditError(error: Throwable?) = _fileEditState.update {
        it.copy(error = error)
    }

    fun renameFile(context: Context) {
        runCatching {
            val oldFile = _fileEditState.value.originalFile!!
            val newFile = oldFile.withNewName(_fileEditState.value.value)
            Writer.renameFile(old = oldFile, new = newFile)
        }.onSuccess {
            updateFiles(context = context)
        }.onFailure {
            setError(it)
        }
    }

    fun deleteFile(context: Context) {
        runCatching {
            Writer.deleteFile(file = _fileEditState.value.originalFile!!)
        }.onSuccess {
            updateFiles(context = context)
        }.onFailure {
            setError(it)
        }
    }

    //endregion

    //region New Folder State
    private val _newFolderState = MutableStateFlow(NewFolderState())
    val newFolderState = _newFolderState.asStateFlow()

    fun showNewFolderDialog() = _newFolderState.update { it.copy(show = true) }
    fun hideNewFolderDialog() = _newFolderState.update {
        it.copy(show = false, value = "", error = null)
    }

    fun updateNewFolderName(value: String) = _newFolderState.update { it.copy(value = value) }
    fun setNewFolderError(error: Throwable?) = _state.update { it.copy(error = error) }

    fun createNewFolder(context: Context) {
        runCatching {
            Writer.createDir(context = context, filepath = _newFolderState.value.value)
        }.onFailure { error ->
            Log.e(TAG, "Couldn't create directory")
            _state.update { it.copy(error = error) }
        }.onSuccess {
            updateNewFolderName("")
            updateFiles(context = context)
        }
    }

    //endregion


    private val _state = MutableStateFlow(NotesScreenState())
    val state = _state.asStateFlow()

    private fun setError(error: Throwable?) {
        _state.update { it.copy(error = error) }
    }

    fun showNewFileSheet() {
        _state.update { it.copy(showNewFileBottomSheet = true) }
    }

    fun hideNewFileSheet() {
        _state.update { it.copy(showNewFileBottomSheet = false) }
    }


    fun updateFiles(context: Context) = _state.update {
        it.copy(
            files = TodoucanFiles(context.homeDir),
            fileTree = getFileTree(context = context),
            fileMap = getFileMap(context = context),
            loading = false
        )
    }


    fun createNewUntitledFile(context: Context, parent: String) {
        runCatching {
            Writer.createUntitledFile(context = context, parent = parent)
        }.onSuccess {
            updateFiles(context = context)
        }.onFailure { error ->
            Log.e(TAG, "Couldn't create new file")
            setError(error)
        }
    }

}