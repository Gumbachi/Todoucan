package com.gumbachi.todoucan.module.tasks

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

private const val TAG = "TASKS-VM"

data class TasksScreenState(
    val loading: Boolean = false,
    val error: Throwable? = null,
)

class TasksScreenViewModel : ViewModel() {

    //region Folder Edit State
    private val _state = MutableStateFlow(TasksScreenState())
    val state = _state.asStateFlow()



}