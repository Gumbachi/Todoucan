package com.gumbachi.todoucan.module.edit

import android.content.Context
import android.util.Log
import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import com.gumbachi.todoucan.data.model.enums.EditMode
import com.gumbachi.todoucan.util.MarkdownPatterns
import com.gumbachi.todoucan.util.fileio.Reader
import com.gumbachi.todoucan.util.fileio.Writer
import com.gumbachi.todoucan.util.fileio.getRelativePath
import com.gumbachi.todoucan.util.fileio.withNewName
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.io.File

private const val TAG = "EDIT-VM"

data class EditScreenState(
    val loading: Boolean = false,
    val error: Throwable? = null,

    val originalFile: File? = null,

    val title: TextFieldValue = TextFieldValue(""),
    val body: TextFieldValue = TextFieldValue(""),

    val isSaved: Boolean = true,
    val showSaveDialog: Boolean = false,
    val showToolbox: Boolean = false,

    val currentEditMode: EditMode = EditMode.Edit
) {

}

class EditScreenViewModel : ViewModel() {

    private val _state = MutableStateFlow(EditScreenState())
    val state = _state.asStateFlow()

    fun setError(error: Throwable?) {
        _state.update { it.copy(error = error) }
    }

    fun toggleToolbox() = _state.update { it.copy(showToolbox = !it.showToolbox) }

    fun showSaveDialog() = _state.update { it.copy(showSaveDialog = true) }
    fun hideSaveDialog() = _state.update { it.copy(showSaveDialog = false) }


    fun updateTitle(title: TextFieldValue) = _state.update { it.copy(title = title, isSaved = false) }
    fun updateText(text: TextFieldValue) = _state.update { it.copy(body = text, isSaved = false) }


    fun toggleStrikethrough(lineIndex: Int) {
        val lines = _state.value.body.text
            .split("\n")
            .toMutableList()

        val line = lines[lineIndex]

        val newString = if (line.matches(MarkdownPatterns.strikethroughWholeLine)) {
            line.removePrefix("~~ ").removeSuffix(" ~~")
        } else {
            "~~ $line ~~"
        }

        lines[lineIndex] = newString

        _state.update {
            it.copy(
                body = TextFieldValue(
                    lines.joinToString("\n")
                )
            )
        }

    }

    fun toggleCheckbox(lineIndex: Int) {

        val lines = _state.value.body.text
            .split("\n")
            .toMutableList()

        val line = lines[lineIndex]

        val newString = if (line.contains(MarkdownPatterns.checkbox)) {
            line.replace(MarkdownPatterns.checkbox, "")
        } else {
            "- [ ] $line"
        }


        lines[lineIndex] = newString

        _state.update {
            it.copy(
                body = TextFieldValue(
                    lines.joinToString("\n")
                )
            )
        }
    }

    fun deleteLine(lineIndex: Int) = _state.update {
        it.copy(
            body = TextFieldValue(
                _state.value.body.text
                    .split("\n")
                    .filterIndexed { index, _ -> index != lineIndex }
                    .joinToString("\n")
            )
        )
    }


    fun setEditMode(mode: EditMode) = _state.update { it.copy(currentEditMode = mode) }

    fun openFile(context: Context, filepath: String) {
        runCatching { Reader.getFile(context = context, filepath = filepath) }
            .onSuccess { file ->
                _state.update {
                    it.copy(
                        title = TextFieldValue(file.name),
                        body = TextFieldValue(file.readText()),
                        originalFile = file
                    )
                }
            }
            .onFailure { error ->
                setError(error)
            }
    }

    fun deleteFile(context: Context) {
        runCatching {
            _state.value.originalFile?.let {
                Writer.deleteFile(context = context, filepath = it.getRelativePath(context))
            }
        }.onSuccess {
            _state.update { it.copy(originalFile = null) }
        }.onFailure {
            Log.d(TAG, "Couldn't delete file ${_state.value.originalFile}")
        }
    }


    fun saveFile(context: Context) {

        if (_state.value.title.text.isEmpty()) {
            setError(Exception("Title cannot be blank"))
            return
        }

        // Rename file if needed
        state.value.originalFile?.let { oldFile ->
            if (oldFile.name != _state.value.title.text) {
                runCatching {
                    Writer.renameFile(
                        context = context,
                        oldPath = oldFile.getRelativePath(context),
                        newPath = oldFile
                            .withNewName(_state.value.title.text)
                            .getRelativePath(context)
                    )
                }.onSuccess { newFile ->
                    _state.update { it.copy(originalFile = newFile ) }
                }.onFailure {
                    setError(Exception("Couldn't rename file"))
                }
            }
        }

        runCatching {
            Writer.writeToFile(
                file = _state.value.originalFile!!,
                content = _state.value.body.text
            )
        }.onSuccess {
            _state.update { it.copy(isSaved = true) }
        }.onFailure {
            setError(it)
        }

    }
}