package com.gumbachi.todoucan.module.notes

import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.ui.components.CalendarFileCatalog
import com.gumbachi.todoucan.ui.components.FolderItem
import com.gumbachi.todoucan.ui.components.LoadingScreen
import com.gumbachi.todoucan.ui.components.NoteItem
import com.gumbachi.todoucan.ui.components.dialog.EditDialog
import com.gumbachi.todoucan.ui.components.dialog.TextFieldDialog
import com.gumbachi.todoucan.ui.components.sheet.AddFileBottomSheet
import com.gumbachi.todoucan.util.getTodayISODate
import com.gumbachi.todoucan.util.getTomorrowISODate
import com.gumbachi.todoucan.util.getYesterdayISODate
import org.koin.androidx.compose.koinViewModel

private const val TAG = "NOTES-SCREEN"

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun NotesScreen(
    modifier: Modifier = Modifier,
    viewModel: NotesScreenViewModel = koinViewModel(),
    onMenuClick: () -> Unit = {},
    navigateToEditScreen: (filename: String) -> Unit
) {

    val context = LocalContext.current

    val state by viewModel.state.collectAsState()
    val folderEditState by viewModel.folderEditState.collectAsState()
    val fileEditState by viewModel.fileEditState.collectAsState()
    val newFolderState by viewModel.newFolderState.collectAsState()

    LaunchedEffect(Unit) {
        Log.d(TAG, "Updating Notes")
        viewModel.updateFiles(context = context)
    }

    AnimatedVisibility(visible = state.loading) {
        LoadingScreen()
    }

    Scaffold(
        modifier = modifier,
        bottomBar = {
            BottomAppBar {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    IconButton(onClick = onMenuClick) {
                        Icon(
                            imageVector = Icons.Default.Menu,
                            contentDescription = "Open Navigation"
                        )
                    }

                    Row {
                        IconButton(onClick = { viewModel.showNewFileSheet() }) {
                            Icon(
                                imageVector = Icons.Default.Add,
                                contentDescription = "Create note"
                            )
                        }
                        IconButton(onClick = { /* todo */ }) {
                            Icon(
                                imageVector = Icons.Default.MoreVert,
                                contentDescription = "More Options"
                            )
                        }
                    }
                }
            }
        }
    ) { paddingValues ->

        Column(
            modifier = Modifier
                .padding(paddingValues)
                .padding(horizontal = 8.dp)
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(8.dp),
        ) {

            state.files?.let { files ->
                CalendarFileCatalog(
                    files = files.calendarFiles,
                    onItemClick = { navigateToEditScreen(it.name) },
                    onItemLongClick = { viewModel.showFileEditDialogFor(it) }
                )
            }

//            state.files?.recentFiles?.let { files ->
//                RecentFilesList(
//                    files = files,
//                    onItemClick = { navigateToEditScreen(it.name) }
//                )
//            }

            // Use normal for loop because continue doesn't work with foreach
            // This block draws only folders of home directory
            for ((dir, files) in state.fileMap) {
                if (dir.parentFile == context.filesDir) continue

                FolderItem(
                    name = dir.name,
                    modifier = Modifier.fillMaxWidth(),
                    onAddClick = {
                        viewModel.createNewUntitledFile(context = context, parent = dir.name)
                    },
                    onLongClick = { viewModel.showFolderEditDialogFor(dir) },
                ) {
                    FlowRow(
                        modifier = Modifier
                            .fillMaxWidth()
                            .animateContentSize(),
                        horizontalArrangement = Arrangement.spacedBy(8.dp),
                        verticalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        if (files.isNotEmpty()) {
                            files.forEach { file ->
                                NoteItem(
                                    title = file.name,
                                    description = runCatching { file.readText() }
                                        .getOrDefault("Couldn't read file"),
                                    onClick = { navigateToEditScreen("${dir.name}/${file.name}") },
                                    onLongClick = { viewModel.showFileEditDialogFor(file) }
                                )
                            }
                        } else {
                            Text(text = "This folder is empty")
                        }
                    }
                }
            }

            FlowRow(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {

                // Draw un-foldered files
                state.files?.nonISOFloatingFiles?.let {
                    it.forEach { file ->
                        NoteItem(
                            title = when (file.name) {
                                getYesterdayISODate() -> "Yesterday"
                                getTodayISODate() -> "Today"
                                getTomorrowISODate() -> "Tomorrow"
                                else -> file.name
                            },
                            description = runCatching { file.readText() }
                                .getOrDefault("Couldn't read file"),
                            onClick = { navigateToEditScreen(file.name) },
                            onLongClick = { viewModel.showFileEditDialogFor(file) }
                        )
                    }
                }
            }




            //region Dialogs
            AddFileBottomSheet(
                showIf = state.showNewFileBottomSheet,
                onDismissRequest = { viewModel.hideNewFileSheet() },
                onAddFileClick = {
                    viewModel.hideNewFileSheet()
                    viewModel.createNewUntitledFile(context = context, parent = "")
                },
                onAddFolderClick = {
                    viewModel.hideNewFileSheet()
                    viewModel.showNewFolderDialog()
                }
            )

            // Folder Creation Dialog
            TextFieldDialog(
                showIf = newFolderState.show,
                value = newFolderState.value,
                onValueChange = {
                    viewModel.setNewFolderError(null)
                    viewModel.updateNewFolderName(it)
                },
                title = "Create New Folder",
                onDismissRequest = { viewModel.hideNewFolderDialog() },
                onConfirm = {
                    viewModel.createNewFolder(context = context)
                    viewModel.hideNewFolderDialog()
                },
                onCancel = { viewModel.hideNewFolderDialog() }
            )

            // Folder Edit Dialog
            EditDialog(
                title = "Edit Folder",
                showIf = folderEditState.show,
                value = folderEditState.value,
                onValueChange = { viewModel.updateFolderEditValue(it) },
                onDismissRequest = viewModel::hideFolderEditDialog,
                onConfirm = {
                    viewModel.renameFolder(context = context)
                    viewModel.hideFolderEditDialog()
                },
                onDelete = {
                    viewModel.deleteFolder(context = context)
                    viewModel.hideFolderEditDialog()
                },
                onCancel = viewModel::hideFolderEditDialog
            )

            // File Edit Dialog
            EditDialog(
                title = "Edit File",
                showIf = fileEditState.show,
                value = fileEditState.value,
                onValueChange = { viewModel.updateFileEditValue(it) },
                onDismissRequest = viewModel::hideFileEditDialog,
                onConfirm = {
                    viewModel.renameFile(context)
                    viewModel.hideFileEditDialog()
                },
                onDelete = {
                    viewModel.deleteFile(context = context)
                    viewModel.hideFileEditDialog()
                },
                onCancel = viewModel::hideFileEditDialog
            )
            //endregion

        }
    }
}



