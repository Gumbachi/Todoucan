package com.gumbachi.todoucan.util

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle

object MarkdownPatterns {
    val checkbox = Regex("^- \\[[*x ]\\] ")
    val strikethroughWholeLine = Regex("^~~.*~~$")
    val strikethrough = Regex("~~.*~~")
}



fun String.toRenderedString(): AnnotatedString {

    val matches = MarkdownPatterns.strikethrough.splitToSequence(this)

    return buildAnnotatedString {

        if (this@toRenderedString.matches(MarkdownPatterns.strikethroughWholeLine)) {
            withStyle(SpanStyle(textDecoration = TextDecoration.LineThrough)) {
                append(this@toRenderedString)
            }
        } else {
            append(this@toRenderedString)
        }
    }
}