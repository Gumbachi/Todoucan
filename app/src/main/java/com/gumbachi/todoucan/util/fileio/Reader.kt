package com.gumbachi.todoucan.util.fileio

import android.content.Context
import com.gumbachi.todoucan.PARENT_DIR_NAME
import java.io.File

private const val TAG = "FILE-READER"


object Reader {

    /** check if filename exists by walking entire home directory. Does not include dirs **/
    fun checkFilenameExists(context: Context, name: String): Boolean {
        return context.homeDir.walk().mapNotNull {
            if (it.isDirectory) {
                null
            } else {
                it.name
            }
        }.contains(name)
    }

    /** Fetch a file from todoucan home dir.
    Throws Error if file doesn't exist **/
    fun getFile(context: Context, filepath: String): File {
        val file = File(context.homeDir, filepath)
        if (file.exists())
            return file
        else
            throw Exception("File does not exist: ${file.path}")
    }

}


internal fun getFile(context: Context, filename: String): File {
    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
    return File(notesDir, filename)
}

//internal fun readFile(context: Context, filename: String): String {
//    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
//    val file = File(notesDir, filename)
//    return file.readText()
//}


//internal fun getFileNames(
//    context: Context,
//    extensionFilter: String = ".md",
//    removeExtension: Boolean = false
//): List<String> {
//    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
//    val files = notesDir.listFiles()!!
//    Log.d(TAG, "$files")
//    val filenames = files.map { it.name }.filter { it.endsWith(extensionFilter) }
//    if (removeExtension)
//        return filenames.map { it.removeSuffix(extensionFilter) }
//    return filenames
//}

internal fun getFiles(context: Context): List<File> {
    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
    val files = notesDir.listFiles()
    return files?.toList() ?: emptyList()
}

internal fun getFileTree(context: Context): FileTreeWalk {
    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
    return notesDir.walkTopDown()
}

internal fun getFileMap(context: Context): Map<File, List<File>> {
    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
    return notesDir.walk()
        .filter { it.isDirectory }
        .associateWith { dir -> dir.listFiles()?.filter { !it.isDirectory } ?: emptyList() }
}
