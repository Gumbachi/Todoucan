package com.gumbachi.todoucan.util

import kotlinx.datetime.Clock
import kotlinx.datetime.DatePeriod
import kotlinx.datetime.LocalDate
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.minus
import kotlinx.datetime.number
import kotlinx.datetime.plus
import kotlinx.datetime.toLocalDateTime
import kotlinx.datetime.todayIn
import java.time.Year

internal fun getTodayISODate() = Clock.System.todayIn(TimeZone.currentSystemDefault()).toString()

internal fun getYesterdayISODate(): String {
    val currentTime = Clock.System.todayIn(TimeZone.currentSystemDefault())
    val yesterday = currentTime.minus(DatePeriod(days = 1))
    return yesterday.toString()
}

internal fun getTomorrowISODate(): String {
    val currentTime = Clock.System.todayIn(TimeZone.currentSystemDefault())
    val tomorrow = currentTime.plus(DatePeriod(days = 1))
    return tomorrow.toString()
}

internal fun String.isTodayISODate() = getTodayISODate() == this
internal fun String.isYesterdayISODate() = getYesterdayISODate() == this
internal fun String.isTomorrowISODate() = getTomorrowISODate() == this

internal fun getCurrentMonth() =
    Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()).month

internal fun getCurrentYear() =
    Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()).year

internal fun getCurrentDay() =
    Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()).dayOfMonth

internal fun getCurrentDate() =
    Clock.System.todayIn(TimeZone.currentSystemDefault())

/** returns a list of days in the month with trailing and leading nulls to start on monday
    Example (Month day 1 is on a Wednesday): [ [null null 1 2 3 4 5] ... [28 29 30 null null null null] ]
**/
internal fun getMonthMap(month: Month, year: Int): List<List<Int?>> {

    val isLeap = Year.isLeap(year.toLong())
    val daysInWeek = 7
    val daysInMonth = month.length(isLeap)

    val monthMap = (1..daysInMonth).associateWith {
        LocalDate(year, month.number, it).dayOfWeek
    }

    val leadingDays = monthMap[1]!!.value - 1
    println(leadingDays)
    val trailingDays = daysInWeek - monthMap[daysInMonth]!!.value
    println(trailingDays)

    val daysList =
        List(leadingDays) { null } + List(daysInMonth) { it + 1 } + List(trailingDays) { null }

    return daysList.chunked(daysInWeek)
}