package com.gumbachi.todoucan.util.fileio

import android.content.Context
import android.util.Log
import com.gumbachi.todoucan.PARENT_DIR_NAME
import java.io.File

private const val TAG = "FILE-WRITER"

val Context.homeDir: File
    get() = File(this.filesDir, PARENT_DIR_NAME)

fun File.getRelativePath(context: Context) =
    this.path.removePrefix(context.homeDir.path + "/")

fun File.withNewName(newName: String): File {
    val newPath = this.path.replace(regex = Regex("${this.name}$"), newName)
    return File(newPath)
}

object Writer {

    internal fun createFile(context: Context, filepath: String): File {
        val newFile = File(context.homeDir, filepath)
        val result = newFile.createNewFile()
        if (!result) {
            Log.d(TAG, "Couldn't Create File: $newFile")
            throw Exception("File Already Exists")
        }
        Log.d(TAG, "Created File: $newFile")
        return newFile
    }

    internal fun createDir(context: Context, filepath: String) {
        val newDir = File(context.homeDir, filepath)
        val result = newDir.mkdir()
        if (!result) {
            Log.d(TAG, "Couldn't Create Directory: $newDir")
            throw Exception("Directory could not be created")
        }
        Log.d(TAG, "Created Directory: $newDir")
    }

    internal fun renameFile(context: Context, oldPath: String, newPath: String): File {

        val oldFile = File(context.homeDir, oldPath)
        val newFile = File(context.homeDir, newPath)

        renameFile(oldFile, newFile)
        return newFile
    }

    internal fun renameFile(old: File, new: File) {

        if (new.exists()) {
            throw Exception("Can't rename file to one that already exists")
        }

        val result = old.renameTo(new)

        if (!result) {
            Log.d(TAG, "Couldn't rename file: $old -> $new")
            throw Exception("Couldn't rename file: $old -> $new")
        } else {
            Log.d(TAG, "Renamed File: $old -> $new")
        }
    }




    internal fun deleteFile(context: Context, filepath: String) {
        val fileToDelete = File(context.homeDir, filepath)
        deleteFile(file = fileToDelete)
    }

    fun deleteFile(file: File) {

        if (!file.exists()) {
            Log.d(TAG, "Couldn't delete file because it doesn't exist: $file")
            throw Exception("Couldn't delete file because it doesn't exist: $file")
        }

        if (file.isDirectory) {
            Log.d(TAG, "Couldn't delete file because it is a directory: $file")
            throw Exception("Couldn't delete file because it is a directory: $file")
        }

        val result = file.delete()

        if (!result) {
            Log.d(TAG, "Couldn't delete file: $file")
            throw Exception("Couldn't delete file")
        }

        Log.d(TAG, "Deleted File: $file")
    }

    internal fun deleteDirectory(
        context: Context,
        filepath: String,
        deleteRecursively: Boolean = false
    ) {
        val dirToDelete = File(context.homeDir, filepath)

        if (!dirToDelete.exists()) {
            Log.d(TAG, "Couldn't delete dir because it doesn't exist: $dirToDelete")
            throw Exception("Couldn't delete dir because it doesn't exist: $dirToDelete")
        }

        if (!dirToDelete.isDirectory) {
            Log.d(TAG, "Couldn't delete dir because it is not a directory: $dirToDelete")
            throw Exception("Couldn't delete dir because it is not a directory: $dirToDelete")
        }

        val result = if (deleteRecursively) {
            dirToDelete.deleteRecursively()
        } else {
            dirToDelete.delete()
        }

        if (!result) {
            Log.d(TAG, "Couldn't delete directory: $dirToDelete")
            throw Exception("Couldn't delete directory")
        }

        Log.d(TAG, "Deleted Directory: $dirToDelete")
    }

    internal fun createTodoucanDirectories(context: Context) {
        val notes = File(context.filesDir, PARENT_DIR_NAME)
        if (notes.exists()) {
            Log.d(TAG, "$PARENT_DIR_NAME Folder already exists. Skipping")
            return
        }

        val dirCreated = notes.mkdir()

        if (!dirCreated) {
            Log.e(TAG, "Couldn't Create $PARENT_DIR_NAME Folder")
            throw Exception("Couldn't Create $PARENT_DIR_NAME Folder")
        }

        Log.e(TAG, "Created $PARENT_DIR_NAME folder successfully")
    }

    internal fun createUntitledFile(context: Context, parent: String): File {
        var counter = 1
        var filename = "Note $counter"
        while (Reader.checkFilenameExists(context = context, name = filename)) {
            filename = "Note $counter"
            counter++
        }

        return createFile(
            context = context,
            filepath = "${parent.removeSuffix("/")}/$filename"
        )
    }

    internal fun writeToFile(context: Context, filepath: String, content: String) {
        val file = Reader.getFile(context = context, filepath = filepath)
        writeToFile(file = file, content = content)
    }

    internal fun writeToFile(file: File, content: String) {
        file.writeText(text = content)
        Log.d(TAG, "Wrote to file $file")
    }



}


internal fun writeToFile(context: Context, filename: String, content: String): File {
    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
    val file = File(notesDir, filename)

    if (!file.exists()) {
        file.createNewFile()
    }

    file.writeText(content)

    Log.d(TAG, "Wrote to file")

    return file
}


//internal fun deleteFileIfExists(context: Context, filename: String) {
//    val notesDir = File(context.filesDir, PARENT_DIR_NAME)
//    val file = File(notesDir, filename)
//    if (file.exists()) {
//        file.delete()
//        Log.d(TAG, "File Deleted: $filename")
//    } else {
//        Log.d(TAG, "Could not delete file: $filename")
//    }
//}
//
//internal fun createDirIfNotExists(context: Context, name: String, parent: String) {
//    val parentDir = File(context.filesDir, parent)
//    val newDir = File(parentDir, name)
//    if (newDir.exists()) {
//        Log.d(TAG, "$name Folder already exists")
//    } else {
//        newDir.mkdir()
//        Log.d(TAG, "$name Folder Created")
//    }
//}
//
//internal fun deleteDir(context: Context, name: String) {
//    val parentDir = File(context.filesDir, PARENT_DIR_NAME)
//    val toDelete = File(parentDir, name)
//    toDelete.delete()
//}
//
//internal fun renameDir(context: Context, original: String, new: String) {
//    val parentDir = File(context.filesDir, PARENT_DIR_NAME)
//    val originalDir = File(parentDir, original)
//    originalDir.renameTo(File(parentDir, new))
//}


