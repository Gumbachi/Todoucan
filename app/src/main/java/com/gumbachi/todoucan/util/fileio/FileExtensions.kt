package com.gumbachi.todoucan.util.fileio

import com.gumbachi.todoucan.util.getTodayISODate
import com.gumbachi.todoucan.util.getTomorrowISODate
import com.gumbachi.todoucan.util.getYesterdayISODate
import kotlinx.datetime.LocalDate
import java.io.File

val File.hasISOName: Boolean
    get() = runCatching {
        LocalDate.parse(this.name)
        true
    }.getOrDefault(false)


val String.formattedDate: String
    get() = when (this) {
        getTodayISODate() -> "Today"
        getYesterdayISODate() -> "Yesterday"
        getTomorrowISODate() -> "Tomorrow"
        else -> this
    }