package com.gumbachi.todoucan

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.TaskAlt
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.gumbachi.todoucan.module.edit.navigateToEditScreen
import com.gumbachi.todoucan.module.notes.navigateToNotesScreen
import com.gumbachi.todoucan.module.settings.navigateToSettingsScreen
import com.gumbachi.todoucan.module.tasks.navigateToTasksScreen
import com.gumbachi.todoucan.navigation.TodoucanNavGraph
import com.gumbachi.todoucan.navigation.TodoucanRoutes
import com.gumbachi.todoucan.ui.components.Calendar
import com.gumbachi.todoucan.ui.theme.TodoucanTheme
import com.gumbachi.todoucan.util.fileio.Reader
import com.gumbachi.todoucan.util.fileio.Writer
import kotlinx.coroutines.launch
import org.koin.compose.KoinContext

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        Writer.createTodoucanDirectories(context = applicationContext)
        super.onCreate(savedInstanceState)

        setContent {
            KoinContext {
                TodoucanTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Todoucan()
                    }
                }
            }
        }
    }
}

@Composable
fun Todoucan() {

    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val navController = rememberNavController()
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    val currentDestination = navController.currentBackStackEntryAsState()

    ModalNavigationDrawer(
        drawerContent = {
            ModalDrawerSheet {
                NavDrawerSectionTitle(text = "Home", showDivider = false)
                NavigationDrawerItem(
                    label = { Text(text = "Notes") },
                    icon = { Icon(Icons.Default.Edit, contentDescription = "Notes") },
                    selected = currentDestination.value?.destination?.route == "notes",
                    onClick = {
                        scope.launch { drawerState.close() }
                        navController.navigateToNotesScreen()
                    },
                    modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                )

                NavigationDrawerItem(
                    label = { Text(text = "Tasks") },
                    icon = { Icon(Icons.Default.TaskAlt, contentDescription = "Tasks") },
                    selected = currentDestination.value?.destination?.route == "tasks",
                    onClick = {
                        scope.launch { drawerState.close() }
                        navController.navigateToTasksScreen()
                    },
                    modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                )

                NavDrawerSectionTitle(text = "Calendar")

                Calendar(onDayClick = {
                    if (!Reader.checkFilenameExists(context = context, name = it.toString())) {
                        Writer.createFile(context = context, filepath = it.toString())
                    }
                    scope.launch { drawerState.close() }
                    navController.navigateToEditScreen(filepath = it.toString())
                })

                NavDrawerSectionTitle(text = "Other")

                NavigationDrawerItem(
                    label = { Text(text = "Settings") },
                    icon = { Icon(Icons.Default.Settings, contentDescription = "Settings") },
                    selected = currentDestination.value?.destination?.route == TodoucanRoutes.Settings.route,
                    onClick = {
                        navController.navigateToSettingsScreen()
                        scope.launch { drawerState.close() }
                    },
                    modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
                )

            }
        },
        drawerState = drawerState,
        gesturesEnabled = true,
    ) {

        TodoucanNavGraph(
            navController = navController,
            modifier = Modifier.fillMaxSize(),
            openNavDrawer = {
                scope.launch { drawerState.open() }
            }
        )
    }
}

@Composable
fun NavDrawerSectionTitle(
    text: String,
    modifier: Modifier = Modifier,
    showDivider: Boolean = true,
) {
    Row(
        modifier = modifier.padding(horizontal = 28.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        Text(
            text = text,
            style = MaterialTheme.typography.titleSmall
        )
        if (showDivider)
            HorizontalDivider()
    }
}
