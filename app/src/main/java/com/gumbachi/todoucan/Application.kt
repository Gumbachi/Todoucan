package com.gumbachi.todoucan

import android.app.Application
import com.gumbachi.todoucan.module.edit.EditScreenViewModel
import com.gumbachi.todoucan.module.notes.NotesScreenViewModel
import com.gumbachi.todoucan.module.settings.SettingsScreenViewModel
import com.gumbachi.todoucan.module.tasks.TasksScreenViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.context.startKoin
import org.koin.dsl.module

const val PARENT_DIR_NAME = "Notes"

val appModule = module {
    // view models
    viewModelOf(::NotesScreenViewModel)
    viewModelOf(::SettingsScreenViewModel)
    viewModelOf(::EditScreenViewModel)
    viewModelOf(::TasksScreenViewModel)
    // repositories


}

class TodoucanApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TodoucanApp)
            modules(appModule)
        }
    }
}
