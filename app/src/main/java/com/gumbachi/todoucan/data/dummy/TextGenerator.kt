package com.gumbachi.todoucan.data.dummy

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import kotlin.random.Random

private const val letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

fun generateText(wordCount: Int): String {
    return List(wordCount) {
        List(Random.nextInt(3, 10)) {
            letters.random()
        }.joinToString { "" }
    }.joinToString { " " }
}

const val previewShortString = "Short String"
const val previewMediumString = "Long String Long String Long String Long String Long String Long String Long String" +
        " Long String Long String Long String Long String Long String"
const val previewLongString = "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
        "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
        "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
        "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String"

class StringProvider : PreviewParameterProvider<String> {
    override val values = listOf(
        "Short String",
        "Long String Long String Long String Long String Long String Long String Long String" +
                " Long String Long String Long String Long String Long String",
        "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
                "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
                "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String " +
                "Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String Extra Long String"
    ).asSequence()
}

