package com.gumbachi.todoucan.data.model

data class TDLSettings(
    val useStrikethroughOnCompleted: Boolean
)
