package com.gumbachi.todoucan.data.dummy

import com.gumbachi.todoucan.data.model.TDLMultiStepObjective
import com.gumbachi.todoucan.data.model.TDLObjective
import kotlin.random.Random

object Dummy {
    val checklist  = listOf(
        TDLMultiStepObjective(
            title = "Random Objective",
            completed = false,
            steps = List(5) {
                TDLObjective(
                    title = "Step $it",
                    description = generateText(Random.nextInt(3, 20)),
                    completed = false
                )
            }
        )
    )
}