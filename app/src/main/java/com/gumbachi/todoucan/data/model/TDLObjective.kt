package com.gumbachi.todoucan.data.model

data class TDLObjective(
    val title: String,
    val description: String,
    val completed: Boolean
)
