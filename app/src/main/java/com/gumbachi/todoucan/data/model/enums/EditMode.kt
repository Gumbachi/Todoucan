package com.gumbachi.todoucan.data.model.enums

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.RemoveRedEye
import androidx.compose.ui.graphics.vector.ImageVector

enum class EditMode(val icon: ImageVector) {
    Edit(Icons.Default.Edit), Read(Icons.Default.RemoveRedEye)
}