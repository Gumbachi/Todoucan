package com.gumbachi.todoucan.data.model

import com.gumbachi.todoucan.util.fileio.hasISOName
import kotlinx.datetime.LocalDate
import java.io.File

data class TodoucanFiles(
    private val homeDir: File
) {

    private val floatingFiles = homeDir.walk()
        .filter { it.isFile && it.parentFile == homeDir }

    val nonISOFloatingFiles = floatingFiles
        .filter { !it.hasISOName }
        .toList()

    val calendarFiles = floatingFiles
        .filter { it.hasISOName }
        .sortedBy { LocalDate.parse(it.name) }
        .toList()

    val recentFiles = homeDir.walk()
        .filter { it.isFile }
        .sortedByDescending { it.lastModified() }
        .toList()

    fun getF() {
        homeDir.walkTopDown().onLeave {  }
    }

}
