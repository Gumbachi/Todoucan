package com.gumbachi.todoucan.data.model

data class TDLMultiStepObjective(
    val title: String,
    val completed: Boolean,
    val steps: List<TDLObjective>
)
