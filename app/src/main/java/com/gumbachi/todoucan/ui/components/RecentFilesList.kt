package com.gumbachi.todoucan.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Note
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import java.io.File

@Composable
fun RecentFilesList(
    files: List<File>,
    modifier: Modifier = Modifier,
    onItemClick: (File) -> Unit
) {

    Column(modifier = modifier.height(250.dp)) {
        Text(text = "Recently Edited", style = MaterialTheme.typography.titleLarge)
        Spacer(modifier = Modifier.height(8.dp))
        LazyColumn(
            modifier = Modifier.fillMaxWidth()
        ) {
            items(files) { file ->
                Row(modifier = Modifier.clickable { onItemClick(file) }) {
                    Icon(imageVector = Icons.AutoMirrored.Filled.Note, contentDescription = "Note")
                    Column {
                        Text(text = file.name, style = MaterialTheme.typography.labelLarge)
                        Text(text = file.lastModified().toString(), style = MaterialTheme.typography.bodyMedium)
                    }
                }
                HorizontalDivider()
            }
        }
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    Surface {
        RecentFilesList(files = listOf(
            File("/hi"),
            File("/howdy"),
            File("/hi"),
            File("/howdy")
        ), onItemClick = {})
    }
}