package com.gumbachi.todoucan.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.util.MarkdownPatterns
import com.gumbachi.todoucan.util.toRenderedString

@Composable
fun InteractiveView(
    content: String,
    onStrikethroughClick: (Int) -> Unit,
    onDeleteLineClick: (Int) -> Unit,
    onToggleCheckboxClick: (Int) -> Unit,
    modifier: Modifier = Modifier
) {
    val lines = content.split("\n")

    var showDropdown by remember { mutableIntStateOf(-1) }

    Column(modifier = modifier.padding(vertical = 8.dp)) {
        lines.forEachIndexed { index, line ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(4.dp)
            ) {
                Text(
                    text = "${index + 1}",
                    style = MaterialTheme.typography.labelSmall,
                    modifier = Modifier.padding(horizontal = 8.dp)
                )

                if (line.contains(MarkdownPatterns.checkbox)) {
                    Checkbox(checked = false, onCheckedChange = {})
                }


                Box {

                    Text(
                        text = line.toRenderedString(),
                        modifier = Modifier.clickable { showDropdown = index }
                    )


                    DropdownMenu(
                        expanded = showDropdown == index,
                        onDismissRequest = { showDropdown = -1 }) {
                        DropdownMenuItem(
                            text = { Text(text = "Strikethrough") },
                            onClick = {
                                onStrikethroughClick(index)
                                showDropdown = -1
                            }
                        )
                        DropdownMenuItem(
                            text = { Text(text = "Delete") },
                            onClick = {
                                onDeleteLineClick(index)
                                showDropdown = -1
                            }
                        )
                        DropdownMenuItem(
                            text = { Text(text = "Checkbox") },
                            onClick = {
                                onToggleCheckboxClick(index)
                                showDropdown = -1
                            }
                        )
                    }
                }
            }
        }
    }
}


@Preview
@Composable
private fun Preview() {

    val content = listOf<String>(
        "# H1 Text Title",
        "## H2 Text Title 2",
        "### H3 Text Whatever",
        "- [ ]"
    )

}