package com.gumbachi.todoucan.ui.components.dialog

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.gumbachi.todoucan.ui.theme.TodoucanTheme

@Composable
fun ErrorDialog(
    error: Throwable,
    onDismissRequest: () -> Unit,
    onConfirm: () -> Unit,
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        title = { Text(text = error::class.simpleName ?: "Error") },
        text = { Text(text = error.message.toString()) },
        confirmButton = {
            TextButton(onClick = onConfirm) {
                Text(text = "Okay")
            }
        },
    )
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        ErrorDialog(error = NullPointerException("Hi"), onDismissRequest = { /*TODO*/ }) {
            
        }
    }
}