package com.gumbachi.todoucan.ui.components

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.data.dummy.StringProvider
import com.gumbachi.todoucan.ui.theme.TodoucanTheme

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FolderItem(
    name: String,
    modifier: Modifier = Modifier,
    onAddClick: () -> Unit,
    onLongClick: () -> Unit,
    locked: Boolean = false,
    content: @Composable () -> Unit,
) {

    var expanded by remember { mutableStateOf(true) }
    val arrowRotation: Float by animateFloatAsState(
        targetValue = if (expanded) 0F else 180F,
        label = "arrow Rotation"
    )
    val borderRoundness: Dp by animateDpAsState(
        targetValue = if (expanded) 0.dp else 8.dp,
        label = "border roundness"
    )

    Column(
        modifier = modifier
            .animateContentSize()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .clip(
                    shape = RoundedCornerShape(
                        topEnd = 8.dp,
                        topStart = 8.dp,
                        bottomStart = borderRoundness,
                        bottomEnd = borderRoundness
                    )
                )
                .background(MaterialTheme.colorScheme.surfaceContainer)
                .combinedClickable(
                    onClick = { expanded = !expanded },
                    onLongClick = onLongClick
                )
                .padding(8.dp)
        ) {
            Text(
                text = name,
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.weight(1F)
            )
            Row(verticalAlignment = Alignment.CenterVertically) {
                if (!locked) {
                    IconButton(onClick = {
                        if (!expanded) expanded = true
                        onAddClick()
                    }) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = "Add File to Folder"
                        )
                    }
                } else {
                    Icon(
                        imageVector = Icons.Default.Lock,
                        contentDescription = "Cannot Edit"
                    )
                }

                IconButton(onClick = { expanded = !expanded }) {
                    Icon(
                        imageVector = Icons.Default.ExpandLess,
                        contentDescription = "Expand",
                        modifier = Modifier.rotate(degrees = arrowRotation)
                    )
                }
            }

        }
        if (expanded) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(bottomEnd = 8.dp, bottomStart = 8.dp))
                    .background(MaterialTheme.colorScheme.surfaceContainer)
                    .padding(horizontal = 8.dp, vertical = 8.dp)
            ) {
                content()
            }
        }
    }
}


@PreviewLightDark
@Composable
private fun Preview(
    @PreviewParameter(StringProvider::class) description: String
) {
    TodoucanTheme {
        Surface {
            FolderItem(name = description, onAddClick = {}, onLongClick = {}, locked = true) {
                Text(text = "Howdy")
            }
        }
    }
}