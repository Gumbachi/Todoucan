package com.gumbachi.todoucan.ui.components.dialog

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.gumbachi.todoucan.ui.theme.TodoucanTheme

@Composable
fun SaveDialog(
    showIf: Boolean,
    onDismissRequest: () -> Unit,
    onConfirmSave: () -> Unit,
    onDismissSave: () -> Unit
) {
    if (showIf) {
        AlertDialog(
            onDismissRequest = onDismissRequest,
            text = { Text(text = "Your edits have not been saved") },
            confirmButton = {
                TextButton(onClick = onConfirmSave) {
                    Text(text = "Save and exit")
                }
            },
            dismissButton = {
                TextButton(onClick = onDismissSave) {
                    Text(text = "Exit without saving", color = Color.Red)
                }
            }
        )
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        SaveDialog(showIf = true, onDismissRequest = { /*TODO*/ }, onConfirmSave = { /*TODO*/ }) {}
    }
}