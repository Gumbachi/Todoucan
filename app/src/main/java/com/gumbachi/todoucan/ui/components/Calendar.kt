package com.gumbachi.todoucan.ui.components

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.RestartAlt
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.ui.theme.TodoucanTheme
import com.gumbachi.todoucan.util.fileio.Reader
import com.gumbachi.todoucan.util.getCurrentDate
import com.gumbachi.todoucan.util.getCurrentMonth
import com.gumbachi.todoucan.util.getCurrentYear
import com.gumbachi.todoucan.util.getMonthMap
import kotlinx.datetime.LocalDate
import kotlinx.datetime.Month

@Composable
fun Calendar(
    modifier: Modifier = Modifier,
    onDayClick: (LocalDate) -> Unit,
) {

    val context = LocalContext.current

    var month by rememberSaveable { mutableStateOf(getCurrentMonth()) }
    var year by rememberSaveable { mutableIntStateOf(getCurrentYear()) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier.animateContentSize()
    ) {
        // Month Selector
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                IconButton(onClick = {
                    if (month == Month.JANUARY) year--
                    month -= 1
                }) {
                    Icon(
                        imageVector = Icons.Default.ChevronLeft,
                        contentDescription = "Previous Month"
                    )
                }
                Text(
                    text = month.name,
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary,
                    modifier = Modifier.requiredWidth(80.dp),
                    textAlign = TextAlign.Center
                )
                IconButton(onClick = {
                    if (month == Month.DECEMBER) year++
                    month += 1
                }) {
                    Icon(
                        imageVector = Icons.Default.ChevronRight,
                        contentDescription = "Next Month"
                    )
                }
            }

            Row(verticalAlignment = Alignment.CenterVertically) {
                IconButton(onClick = { year -= 1 }) {
                    Icon(
                        imageVector = Icons.Default.ChevronLeft,
                        contentDescription = "Previous Month"
                    )
                }
                Text(
                    text = year.toString(),
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary
                )
                IconButton(onClick = { year += 1 }) {
                    Icon(
                        imageVector = Icons.Default.ChevronRight,
                        contentDescription = "Next Month"
                    )
                }
            }
        }

        // Weekday
        Row {
            "MTWTFSS".forEach {
                IconButton(
                    onClick = {},
                    enabled = false
                ) {
                    Text(text = it.toString())
                }
            }
        }

        val daymap by remember { derivedStateOf { getMonthMap(month, year) } }

        daymap.forEach { week ->
            Row {
                week.forEach {

                    val date = it?.let { LocalDate(year, month, it) }

                    IconButton(
                        onClick = { onDayClick(date!!) },
                        enabled = it != null,
                        modifier = when (date != null && getCurrentDate() == date) {
                            true -> Modifier
                                .padding(2.dp)
                                .border(
                                    width = 2.dp,
                                    color = MaterialTheme.colorScheme.primary,
                                    shape = CircleShape
                                )

                            false -> Modifier
                        }

                    ) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            Text(
                                text = it?.toString() ?: " ",
                                style = MaterialTheme.typography.titleMedium
                            )
                            if (it != null &&
                                Reader.checkFilenameExists(
                                    context = context,
                                    name = date.toString()
                                )
                            ) {
                                Box(
                                    modifier = Modifier
                                        .size(4.dp)
                                        .clip(CircleShape)
                                        .background(MaterialTheme.colorScheme.primary)
                                )
                            }
                        }
                    }
                }
            }
        }

        Button(
            onClick = {
                year = getCurrentYear()
                month = getCurrentMonth()
            },
            enabled = year != getCurrentYear() || month != getCurrentMonth()
        ) {
            Icon(imageVector = Icons.Default.RestartAlt, contentDescription = "Rest Calendar")
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Reset Calendar")
        }

    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        Surface {
            Calendar {}
        }
    }
}