package com.gumbachi.todoucan.ui.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue

@Composable
fun EditView(
    modifier: Modifier = Modifier
) {

    var text by remember { mutableStateOf(TextFieldValue("")) }


}