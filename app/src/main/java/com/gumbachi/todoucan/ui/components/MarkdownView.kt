package com.gumbachi.todoucan.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun MarkdownView(
    content: String,
    modifier: Modifier = Modifier
) {
    val lines = content.split("\n")

    Column {
        lines.forEachIndexed { index, line ->
            Row {
                Text(text = "${index + 1}")
                Text(text = line)
            }
        }
    }
}


@Preview
@Composable
private fun Preview() {

    val content = listOf<String>(
        "# H1 Text Title",
        "## H2 Text Title 2",
        "### H3 Text Whatever",
        "- [ ]"
    )

}