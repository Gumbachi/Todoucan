package com.gumbachi.todoucan.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.util.fileio.formattedDate
import java.io.File

@Composable
fun CalendarFileCatalog(
    files: List<File>,
    modifier: Modifier = Modifier,
    onItemClick: (File) -> Unit,
    onItemLongClick: (File) -> Unit,
) {

    Column(modifier = modifier) {
        Text(text = "Calendar Notes", style = MaterialTheme.typography.titleLarge)
        Spacer(modifier = Modifier.height(8.dp))
        LazyRow(
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier.fillMaxWidth()
        ) {
            items(files) { file ->
                NoteItem(
                    title = file.name.formattedDate,
                    description = runCatching { file.readText() }
                        .getOrDefault("Couldn't Read File"),
                    onClick = { onItemClick(file) },
                    onLongClick = { onItemLongClick(file) }
                )
            }
        }
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    Surface {
        CalendarFileCatalog(files = listOf(
            File("/hi"),
            File("/howdy"),
            File("/hi"),
            File("/howdy")
        ), onItemClick = {}, onItemLongClick = {})
    }
}