package com.gumbachi.todoucan.ui.components

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckBox
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun Toolbox(
    expanded: Boolean,
    onExpandClick: () -> Unit,
    modifier: Modifier = Modifier,
    onAddCheckboxClick: () -> Unit = {}
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .animateContentSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Button(
            onClick = onExpandClick,
            shape = RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp),
            contentPadding = PaddingValues(horizontal = 64.dp, vertical = 0.dp),
            modifier = Modifier.offset(y = 4.dp)
        ) {
            if (!expanded) {
                Icon(imageVector = Icons.Default.ExpandLess, contentDescription = "Open Toolbox")
            } else {
                Icon(imageVector = Icons.Default.ExpandMore, contentDescription = "Close Toolbox")
            }
        }

        if (expanded) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colorScheme.primary)
            ) {
                IconButton(
                    onClick = onAddCheckboxClick
                ) {
                    Icon(
                        imageVector = Icons.Default.CheckBox,
                        contentDescription = "Add Checkbox",
                        tint = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
    }
}

@Preview
@Composable
private fun Preview() {
    Toolbox(expanded = true, onExpandClick = {})
}