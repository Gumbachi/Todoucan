package com.gumbachi.todoucan.ui.components.sheet

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.InsertDriveFile
import androidx.compose.material.icons.filled.Folder
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import com.gumbachi.todoucan.ui.theme.TodoucanTheme

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun AddFileBottomSheet(
    showIf: Boolean,
    modifier: Modifier = Modifier,
    onDismissRequest: () -> Unit,
    onAddFileClick: () -> Unit,
    onAddFolderClick: () -> Unit
) {
    if (showIf) {
        ModalBottomSheet(
            modifier = modifier,
            onDismissRequest = onDismissRequest
        ) {
            FlowRow(
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier.fillMaxWidth()
            ) {
                Button(
                    onClick = onAddFileClick,
                    contentPadding = PaddingValues(vertical = 32.dp, horizontal = 16.dp),
                    shape = RoundedCornerShape(12.dp),
                    modifier = Modifier.fillMaxWidth(0.4F)
                ) {
                    Icon(
                        imageVector = Icons.AutoMirrored.Filled.InsertDriveFile,
                        contentDescription = "Add File"
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(text = "New File", style = MaterialTheme.typography.titleMedium)
                }
                Button(
                    onClick = onAddFolderClick,
                    contentPadding = PaddingValues(vertical = 32.dp, horizontal = 16.dp),
                    shape = RoundedCornerShape(12.dp),
                    modifier = Modifier.fillMaxWidth(0.4F)
                ) {
                    Icon(
                        imageVector = Icons.Default.Folder,
                        contentDescription = "Add Folder"
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(text = "New Folder", style = MaterialTheme.typography.titleMedium)
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        Surface {
            AddFileBottomSheet(
                showIf = true,
                onDismissRequest = { /*TODO*/ },
                onAddFileClick = { /*TODO*/ }) {
            }
        }

    }
}