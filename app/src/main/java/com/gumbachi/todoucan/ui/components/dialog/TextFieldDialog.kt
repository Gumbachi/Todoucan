package com.gumbachi.todoucan.ui.components.dialog

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.gumbachi.todoucan.ui.theme.TodoucanTheme

@Composable
fun TextFieldDialog(
    showIf: Boolean,
    value: String,
    onValueChange: (String) -> Unit,
    title: String,
    modifier: Modifier = Modifier,
    onDismissRequest: () -> Unit,
    onConfirm: () -> Unit,
    onCancel: () -> Unit,
    isError: Boolean = false,
    confirmText: String = "Confirm",
    cancelText: String = "Cancel",
    singleLine: Boolean = true,
) {

    if (showIf) {
        AlertDialog(
            onDismissRequest = onDismissRequest,
            modifier = modifier,
            title = { Text(text = title) },
            text = {
                OutlinedTextField(
                    value = value,
                    onValueChange = onValueChange,
                    isError = isError,
                    singleLine = singleLine,
                    keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences)
                )
            },
            confirmButton = {
                TextButton(onClick = onConfirm) {
                    Text(text = confirmText)
                }
            },
            dismissButton = {
                TextButton(onClick = onCancel) {
                    Text(text = cancelText)
                }
            },
        )
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        TextFieldDialog(
            showIf = true,
            value = "abc",
            onValueChange = {},
            title = "Title",
            onDismissRequest = { /*TODO*/ },
            onConfirm = { /*TODO*/ },
            onCancel = { /*TODO*/ })
    }
}