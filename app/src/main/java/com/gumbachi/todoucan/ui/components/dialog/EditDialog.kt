package com.gumbachi.todoucan.ui.components.dialog

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.gumbachi.todoucan.ui.theme.TodoucanTheme


@Composable
fun EditDialog(
    title: String,
    showIf: Boolean,
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    onDismissRequest: () -> Unit,
    onConfirm: () -> Unit,
    onDelete: () -> Unit,
    onCancel: () -> Unit,
    isError: Boolean = false
) {

    if (showIf) {
        AlertDialog(
            modifier = modifier,
            onDismissRequest = onDismissRequest,
            title = {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(text = title)
                    IconButton(onClick = onDelete) {
                        Icon(imageVector = Icons.Default.Delete, contentDescription = "Delete Folder")
                    }
                }
            },
            text = {
                OutlinedTextField(
                    value = value,
                    onValueChange = onValueChange,
                    isError = isError,
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Sentences)
                )
            },
            confirmButton = {
                TextButton(onClick = onConfirm) {
                    Text(text = "Save")
                }
            },
            dismissButton = {
                TextButton(onClick = onCancel) {
                    Text(text = "Cancel")
                }
            },
        )
    }
}

@PreviewLightDark
@Composable
private fun Preview() {
    TodoucanTheme {
        EditDialog(
            title = "Edit Dialog",
            showIf = true,
            value = "",
            onValueChange = {},
            onDismissRequest = {},
            onConfirm = {},
            onDelete = {},
            onCancel = {},
            isError = false
        )
    }
}
