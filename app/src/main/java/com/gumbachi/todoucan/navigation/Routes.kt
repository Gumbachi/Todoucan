package com.gumbachi.todoucan.navigation

interface TodoucanRoute {
    val route: String
}

object TodoucanRoutes {

    data object Notes : TodoucanRoute {
        override val route = "notes"
    }

    data object Settings : TodoucanRoute {
        override val route = "settings"
    }

    data class Draft(val filename: String? = null) : TodoucanRoute {
        override val route = if (filename != null) "draft/$filename" else "draft"
    }


}

