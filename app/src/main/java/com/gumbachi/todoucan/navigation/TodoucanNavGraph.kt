package com.gumbachi.todoucan.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.gumbachi.todoucan.module.edit.editScreen
import com.gumbachi.todoucan.module.edit.navigateToEditScreen
import com.gumbachi.todoucan.module.notes.notesScreen
import com.gumbachi.todoucan.module.settings.settingsScreen
import com.gumbachi.todoucan.module.tasks.tasksScreen

@Composable
fun TodoucanNavGraph(
    navController: NavHostController,
    openNavDrawer: () -> Unit,
    modifier: Modifier = Modifier,
) {

    NavHost(
        navController = navController,
        startDestination = TodoucanRoutes.Notes.route,
        modifier = modifier
    ) {

        notesScreen(
            onMenuClick = openNavDrawer,
            navigateToEditScreen = { navController.navigateToEditScreen(filepath = it) }
        )

        settingsScreen(onMenuClick = openNavDrawer)

        editScreen(onMenuClick = openNavDrawer, navigateBack = { navController.popBackStack() })

        tasksScreen(onMenuClick = openNavDrawer)

    }

}